Customized setup scripts for the HBP Neurorobotics Plattform
------------------------------------------------------------

A fork of https://bitbucket.org/hbpneurorobotics/user-scripts.

Changes:
* $HOME/.local replaced by $HBP_INSTALL_PREFIX
* setup.bash configures runtime environement. Takes a local subdirectory as argument which specifies the install location. See setup_deb.sh and setup_rel.sh for how to use it. In my installation the directory structure is like
NRP/
  src/
    user-scripts
    gazebo
    CLE
    ...
  setup.bash -> src/user/scripts/setup.bash
  setup_rel.sh -> setc/user/scripts/setup_rel.bash
  local/
    rel/   (release builds)
    deb/   (debug builds)

* Selective building: update_nrp build gazebo  (and so on)
* New "update_nrp status" command prints overview of all repositories.
* Aliases turned into functions. Thus, they can be used in sub-shells.
