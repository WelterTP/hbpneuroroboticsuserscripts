from_nrp_variables() {
export ROS_PARALLEL_JOBS=-j4
export ROS_IP=127.0.0.1


# Let gazebo find plugin in the install directory
export GAZEBO_PLUGIN_PATH=$GAZEBO_PLUGIN_PATH:$HBP_INSTALL_PREFIX/lib
# HPB models for gazebo
export GAZEBO_MODEL_PATH=$HOME/.gazebo/models:$GAZEBO_MODEL_PATH


export LD_LIBRARY_PATH=$HBP_INSTALL_PREFIX/lib/x86_64-linux-gnu:$LD_LIBRARY_PATH
#export CMAKE_PREFIX_PATH=$HBP_INSTALL_PREFIX/lib/x86_64-linux-gnu/cmake/gazebo/:$CMAKE_PREFIX_PATH

# Try to tell cmake where to look for libraries
export PKG_CONFIG_PATH=$HBP_INSTALL_PREFIX:$PKG_CONFIG_PATH
export CMAKE_PREFIX_PATH=$HBP_INSTALL_PREFIX:$CMAKE_PREFIX_PATH

# HBP opensim
export OPENSIM_INSTALL_DIR=$HBP_INSTALL_PREFIX
export OPENSIM_HOME=$HBP_INSTALL_PREFIX

# HBP CLE
export PYTHONPATH=$PYTHONPATH:$HBP/CLE/hbp_nrp_cle
# HBP ExperimentControl
export PYTHONPATH=$PYTHONPATH:$HBP/ExperimentControl/hbp_nrp_excontrol:$HBP/ExperimentControl/hbp_nrp_scxml
# HBP ExDBackend
export PYTHONPATH=$PYTHONPATH:$HBP/ExDBackend/hbp_nrp_backend:$HBP/ExDBackend/hbp_nrp_cleserver:$HBP/ExDBackend/hbp_nrp_commons:$HBP/ExDBackend/hbp_nrp_watchdog
# HBP Flask restful Swagger
export PYTHONPATH=$PYTHONPATH:$HBP/ExDBackend/hbp-flask-restful-swagger-master
# HBP VirtualCoach
export PYTHONPATH=$PYTHONPATH:$HBP/VirtualCoach/hbp_nrp_virtual_coach
# HBP MUSIC pyNN/NEST Support
export PYTHONPATH=$PYTHONPATH:$HBP/BrainSimulation/hbp_nrp_music_xml:$HBP/BrainSimulation/hbp_nrp_music_interface
# HBP Distributed NEST Support
export PYTHONPATH=$PYTHONPATH:$HBP/BrainSimulation/hbp_nrp_distributed_nest
# retina
export RETINA_INSTALL_DIR=$HBP/retina
export PYTHONPATH=$RETINA_INSTALL_DIR/build/lib:$PYTHONPATH
# HBP models directory

export NRP_MODELS_DIRECTORY=$HBP/Models
export NRP_EXPERIMENTS_DIRECTORY=$HBP/Experiments

# NRP local storage path
export STORAGE_PATH=$HBP_INSTALL_PREFIX/opt/nrpStorage

# General NRP
export NRP_USER=$USER

# User scripts
export PATH=$HBP/user-scripts:$PATH

export LD_LIBRARY_PATH=$HBP_INSTALL_PREFIX/lib:$LD_LIBRARY_PATH
#export PKG_CONFIG_PATH=$HBP_INSTALL_PREFIX/lib/pkgconfig:$PKG_CONFIG_PATH
export MANPATH=$HBP_INSTALL_PREFIX/share/man:$MANPATH

#echo "Before include paths: HBP_INSTALL_PREFIX=$HBP_INSTALL_PREFIX"
export C_INCLUDE_PATH=$HBP_INSTALL_PREFIX/include:$C_INCLUDE_PATH
export CPLUS_INCLUDE_PATH=$HBP_INSTALL_PREFIX/include:$CPLUS_INCLUDE_PATH
export CPATH=$HBP_INSTALL_PREFIX/include:$CPATH
#echo "After include paths : C_INCLUDE_PATH=$C_INCLUDE_PATH, CPLUS_INCLUDE_PATH=$CPLUS_INCLUDE_PATH, CPATH=$CPATH"

# Make number of make processes user-configurable
if [ -z "$NRP_NUM_CORES" ]; then
    export NRP_NUM_MAKE_PROCESSES=`nproc`
else
    export NRP_NUM_MAKE_PROCESSES=$NRP_NUM_CORES
fi
}


setup_nrp () {
  # Nonstandard HBP env vars.
  local HBP_BUILD_NAME=$1
  if [ $HBP_BUILD_NAME == "rel" ]; then
    export BUILD_TYPE="RelWithDebInfo"
  else
    export BUILD_TYPE="Debug"
  fi
  echo "BUILD_TYPE=${BUILD_TYPE}"

  # TODO: No Welter specific system directories please ...
  export HBP_DIR=/mnt/work/NRP
  export HBP_INSTALL_PREFIX=$HBP_DIR/local/$HBP_BUILD_NAME
  export HBP_BUILD_DIR=/mnt/data/NRP/build/$HBP_BUILD_NAME
    
  export NRP_NUM_CORES=6
  export HBP=$HBP_DIR/src
  export GAZEBO_ROS_NON_RETARD_MODE=1
  echo "HBP_INSTALL_PREFIX=${HBP_INSTALL_PREFIX} - added to search paths"
  echo "HBP_BUILD_DIR=${HBP_BUILD_DIR}"

  # Must not source ROS script because it prevents GazeboRosPackages from overriding ROS include folders.
#   ROS_SETUP_SCRIPT=/mnt/data/ROS/kinetic/install_isolated/setup.bash
#   if [ -f $ROS_SETUP_SCRIPT ]; then
#     source $ROS_SETUP_SCRIPT
#   else
#     echo "Skipped sourcing of ROS setup script '$ROS_SETUP_SCRIPT'. File dos not exist."
#   fi

  GAZEBO_SETUP_FILE=$HBP_INSTALL_PREFIX/share/gazebo/setup.sh
  if [ -f $GAZEBO_SETUP_FILE ]; then
    source $GAZEBO_SETUP_FILE
  else
    echo "Skipped sourcing of Gazebo setup script: $GAZEBO_SETUP_FILE. File does not exist."
  fi

  GAZEBO_ROS_SETUP_SCRIPT=$HBP_INSTALL_PREFIX/setup.bash
  if [ -f $GAZEBO_ROS_SETUP_SCRIPT ]
  then
    echo "Sourcing setup script of GazeboRosPackages: $GAZEBO_ROS_SETUP_SCRIPT"
    source $GAZEBO_ROS_SETUP_SCRIPT
  else
    #GAZEBO_ROS_SETUP_SCRIPT=$HBP/GazeboRosPackages/devel/setup.$CURR_SHELL
    echo "Skipped sourcing of Gazebo ROS workspace setup '$GAZEBO_ROS_SETUP_SCRIPT'. File dos not exist. "
  fi


  source $HBP_INSTALL_PREFIX/bin/activate
  export NRP_VIRTUAL_ENV=$VIRTUAL_ENV



  # Needed for javascript packet manager
  # TODO: Check if the env var is already set. If so omit it here.
  # Because by default nvm should really go into $HOME/.nvm. and NVM_DIR should already be set in .bashrc.
  export NVM_DIR="$HBP/nvm"

  from_nrp_variables

  # Source various command shortcuts
  if [ -f $HBP/user-scripts/nrp_aliases ]; then
    source $HBP/user-scripts/nrp_aliases
  else
    echo "Skipped sourcing of '$HBP/user-scripts/nrp_aliases'. File does not exist."
  fi
}


if [ -z $1 ]; then
  echo "Please give a prefix to name this installation as first argument to this script, e.g.
  > source setup.bash debug"
else
  setup_nrp $1
fi
